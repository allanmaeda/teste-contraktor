This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

# React project Test: development a CRUD.

The test had as its task the simple implementation CRUD of contract and parts. Relate a contract to one or more parties and simple view of a contract with contract-related parties

## Node/NPM Versions

Node is really easy to install and it includes the needed NPM. You can run the following command after the installation procedure below ```node --version v10.15.3``` and ```npm --version 6.4.1 ``` are the versions I am using while developing the test.


## Install
```
$ git clone https://gitlab.com/allanmaeda/teste-contraktor.git
$ cd PROJECT
$ npm install
```

### `npm start

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.

# Backend

You need previously start the Phoenix server and after then React server.



