import React from 'react';
import './styles.css'
const Header = ({ title }) => (
    <header>
        <link href='http://fonts.googleapis.com/css?family=Lato:400,700' rel='stylesheet' type='text/css'></link>
        <h1 className="font-weight-bold"> {title ? title : 'Escolha um título'} </h1>
    </header>
)

export default Header;