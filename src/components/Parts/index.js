import React, { Component } from 'react';
import {
    Table,
    Button,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap'
import PubSub from 'pubsub-js';
import './styles.css'
class FormPart extends Component {

    state = {
        model: {
            id: 0,
            name: "",
            last_name: "",
            email: "",
            cpf: "",
            phone: ""
        }
    };

    setValues = (e, field) => {
        const { model } = this.state;
        model[field] = e.target.value;
        this.setState({ model });


    }

    create = () => {

        this.setState({ model: { id: 0, name: "", last_name: "", email: "", cpf: "", phone: "" } })
        this.props.partCreate(this.state.model)
    }

    componentWillMount() {
        PubSub.subscribe('edit-part', (topic, part) => {
            this.setState({ model: part })
        });
    }


    render() {
        return (
            <Form>
                <FormGroup>
                    <div className="form-row mb-3">
                        <div className="col-md-6">
                            <Label for="name">Nome:</Label>
                            <Input id="name" type="text" value={this.state.model.name} placeholder="Digite o nome"
                                onChange={e => this.setValues(e, 'name')} />
                        </div>
                        <div className="col-md-6">
                            <Label for="last_name">Sobrenome:</Label>
                            <Input id="last_name" type="text" value={this.state.model.last_name} placeholder="Digite o sobrenome"
                                onChange={e => this.setValues(e, 'last_name')} />
                        </div>
                    </div>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <Label for="email">Email:</Label>
                            <Input id="email" type="text" value={this.state.model.email} placeholder="Digite o email" onChange={e => this.setValues(e, 'email')} />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6">
                            <Label for="cpf">CPF:</Label>
                            <Input id="cpf" type="text" value={this.state.model.cpf} placeholder="Digite o CPF" onChange={e => this.setValues(e, 'cpf')} />
                        </div>
                        <div className="col-md-6">
                            <Label for="phone">Telefone:</Label>
                            <Input id="phone" type="text" value={this.state.model.phone} placeholder="Digite o telefone" onChange={e => this.setValues(e, 'phone')} />
                        </div>
                    </div>
                </FormGroup>
                <div className="text-center">
                    <Button color="primary" className="btn" block onClick={this.create}>Salvar</Button>
                </div>
            </Form>
        )
    }
}

class ListPart extends Component {

    delete = (id) => {
        this.props.deletePart(id);
    }

    onEdit = (part) => {
        PubSub.publish('edit-part', part)
    }

    render() {
        const { parts } = this.props;
        return (
            <div>
                <Table className="table table-responsive text-center pt-2">
                    <thead className="thead-light">
                        <tr>
                            <th>Nome</th>
                            <th>Sobrenome</th>
                            <th>Email</th>
                            <th>CPF</th>
                            <th>Telefone</th>
                            <th className="size-large text-center">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            parts.map(part => (
                                <tr key={part.id}>
                                    <td>{part.name}</td>
                                    <td>{part.last_name}</td>
                                    <td>{part.email}</td>
                                    <td>{part.cpf}</td>
                                    <td>{part.phone}</td>
                                    <td className="">

                                        <Button className="btn" color="info" size="sm" onClick={e => this.onEdit(part)}>Editar</Button>
                                        <Button className="btn" color="danger" size="sm" onClick={e => this.delete(part.id)}>Excluir</Button>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default class PartBox extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isLoad: false,
            parts: [],
        };
    }
    Url = 'http://localhost:4000/api/parts'

    componentDidMount() {
        fetch(this.Url)
            .then(response => response.json())
            .then(parts =>
                this.setState({
                    isLoad: true,
                    parts: parts
                }))
            .catch(e => console.log(e))
    }

    create = (part) => {
        var data = {
            id: parseInt(part.id),
            name: part.name,
            last_name: part.last_name,
            email: part.email,
            cpf: part.cpf,
            phone: part.phone,
        }
        const requestInfo = {
            method: data.id !== 0 ? 'PUT' : 'POST',
            body: JSON.stringify(data),
            headers: new Headers({
                'Content-type': 'application/json',
            })
        };
        const opt = {
            method: 'PUT',
            body: JSON.stringify({ part }),
            headers: new Headers({
                'Content-type': 'application/json',
            })
        };


        if (data.id === 0) {
            fetch(this.Url, requestInfo)
                .then(response => response.json())
                .then(newPart => {
                })
                .catch(e => {
                    console.log(e)
                    alert('Salvo com sucesso.')
                    window.location.reload();
                })
        }
        else {
            fetch(`http://localhost:4000/api/parts/${data.id}`, opt)
                .then(response => {

                    return response.json()
                }
                )
                .then(updatedPart => {
                    let { parts } = this.state;
                    let position = parts.findIndex(part => part.id === data.id)
                    parts[position] = updatedPart;
                    this.setState({ parts })

                })
                .catch(e => {
                    alert('Alterado com sucesso.')
                    window.location.reload();
                    console.log(e)
                })
        }


    }

    delete = (id) => {
        const requestOptions = {
            method: 'DELETE'
        };
        fetch(`http://localhost:4000/api/parts/${id}`, requestOptions)
            .then(response => {
                return response.json()
            })
            .then(rows => {
            })
            .catch(e => {
                alert('Deletado com sucesso.')
                window.location.reload();
                console.log(e)

            });


    }
    render() {

        var { isLoad, parts } = this.state;

        if (!isLoad) {
            return <div>Loading...</div>
        }

        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 my-3">
                        <h2 className="font-weight-bold text-center">Cadastro das partes</h2>
                        <FormPart partCreate={this.create} />
                    </div>
                    <div className="col-md-6 my-3">
                        <h2 className="font-weight-bold text-center">Lista das partes</h2>
                        <ListPart parts={parts.data} deletePart={this.delete} />
                    </div>
                </div>
            </div>
        );
    }

}