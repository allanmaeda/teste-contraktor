import React, { Component } from 'react';
import {
    Table,
    Button,
    Form,
    FormGroup,
    Label,
    Input
} from 'reactstrap'
import PubSub from 'pubsub-js';
import './styles.css'
class FormContract extends Component {

    state = {
        model: {
            id: 0,
            title: "",
            date_init: "",
            date_final: "",
            file: "",
        }
    };

    setValues = (e, field) => {
        const { model } = this.state;
        model[field] = e.target.value;
        this.setState({ model });
    }

    create = () => {
        this.setState({ model: { id: 0, title: "", date_init: "", date_final: "", file: "" } })
        this.props.contractCreate(this.state.model)

    }

    componentWillMount() {
        PubSub.subscribe('edit-contract', (topic, contract) => {
            this.setState({ model: contract })
        });
    }
    // onFileChangeHandler = (e) => {
    //     e.preventDefault();
    //     this.setState({
    //         selectedFile: e.target.files[0]
    //     });
    //     this.formData = new FormData();
    //     this.formData.append('file', this.state.selectedFile);
    //     fetch('http://localhost:4000/api/contracts', {
    //         method: 'post',
    //         body: formData
    //     }).then(res => {
    //         if(res.ok) {
    //             console.log(res.data);
    //             alert("File uploaded successfully.")
    //         }
    //     });
    // };

    render() {
        return (
            <Form>
                <FormGroup>
                    <div className="form-row mb-3">
                        <div className="col-md-12">
                            <Label for="title">Titulo:</Label>
                            <Input id="title" type="text" value={this.state.model.title} placeholder="Digite o título" onChange={e => this.setValues(e, 'title')} />
                        </div>
                    </div>
                    <div className="form-row">
                        <div className="col-md-6">
                            <Label for="date_init">Data inicio:</Label>
                            <Input id="date_init" type="date" value={this.state.model.date_init} onChange={e => this.setValues(e, 'date_init')} />
                        </div>
                        <div className="col-md-6 mb-3">
                            <Label for="date_final">Data vencimento:</Label>
                            <Input id="date_final" type="date" value={this.state.model.date_final} onChange={e => this.setValues(e, 'date_final')} />
                        </div>
                    </div>
                    <div className="form-row mb-4">
                        <div className="col-md-12">
                            <Label for="file">Arquivo:</Label>
                            <Input id="file" type="text" value={this.state.model.file} placeholder="Não tive tempo de fazer pegar :(" onChange={e => this.setValues(e, 'file')} />
                        </div>
                    </div>
                </FormGroup>
                <div className="text-center">
                    <Button color="primary" className="btn" block onClick={this.create}>Salvar</Button>
                </div>
            </Form>
        )
    }
}

class ListContract extends Component {

    delete = (id) => {
        this.props.deleteContract(id);
    }

    onEdit = (contract) => {
        PubSub.publish('edit-contract', contract)
    }

    render() {
        const { contracts } = this.props;
        return (
            <div>
                <Table className="table table-responsive text-center pt-2">
                    <thead className="thead-light">
                        <tr>
                            <th className="text-center">Titulo</th>
                            <th>Data inicio</th>
                            <th>Data vencimento</th>
                            <th>Arquivo</th>
                            <th className="text-center">Ação</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            contracts.map(contract => (
                                <tr key={contract.id}>
                                    <td>{contract.title}</td>
                                    <td>{contract.date_init}</td>
                                    <td>{contract.date_final}</td>
                                    <td>{contract.file}</td>
                                    <td className="">
                                        <Button className="btn" color="info" size="sm" onClick={e => this.onEdit(contract)}>Editar</Button>
                                        <Button className="btn" color="danger" size="sm" onClick={e => this.delete(contract.id)}>Excluir</Button>
                                    </td>
                                </tr>
                            ))
                        }
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default class ContractBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isLoad: false,
            contracts: [],
        };
    }

    Url = 'http://localhost:4000/api/contracts'

    componentDidMount() {
        fetch(this.Url)
            .then(response => response.json())
            .then(contracts =>
                this.setState({
                    isLoad: true,
                    contracts: contracts
                }))
            .catch(e => console.log(e))
    }

    create = (contract) => {
        var data = {
            id: parseInt(contract.id),
            title: contract.title,
            date_init: contract.date_init,
            date_final: contract.date_final,
            file: contract.file,
        }
        const requestInfo = {
            method: 'POST',
            body: JSON.stringify(contract),
            headers: new Headers({
                'Content-type': 'application/json',
            })
        };
        const opt = {
            method: 'PUT',
            body: JSON.stringify({ contract }),
            headers: new Headers({
                'Content-type': 'application/json',
            })
        };


        if (data.id === 0) {
        fetch(this.Url, requestInfo)
            .then(response => response.json())
            .then(newContract => {
                let { contracts } = this.state;
                contracts.push(newContract)
                this.setState({ contracts })
            })
            .catch(e => {
                console.log(e)
                alert('Salvo com sucesso.')
                window.location.reload();
            })
        }
        else {
            fetch(`http://localhost:4000/api/contracts/${data.id}`, opt)
                .then(response => {

                    return response.json()
                }
                )
                .then(updatedPart => {
                    let { contracts } = this.state;
                    let position = contracts.findIndex(part => part.id === data.id)
                    contracts[position] = updatedPart;
                    this.setState({ contracts })

                })
                .catch(e => {
                    alert('Alterado com sucesso.')
                    window.location.reload();
                    console.log(e)
                })
        }
    }

    delete = (id) => {
        const requestOptions = {
            method: 'DELETE'
        };
        fetch(`http://localhost:4000/api/contracts/${id}`, requestOptions)
            .then(response => {
                return response.json()
            })
            .then(rows => {
            })
            .catch(e => {
                alert('Deletado com sucesso.')
                window.location.reload();
                console.log(e)

            });


    }

    render() {
        var { isLoad, contracts } = this.state;

        if (!isLoad) {
            return <div>Loading...</div>
        }
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md-6 my-3">
                        <h2 className="font-weight-bold text-center">Cadastro dos Contratos</h2>
                        <FormContract contractCreate={this.create} />
                    </div>
                    <div className="col-md-6 my-3">
                        <h2 className="font-weight-bold text-center">Lista dos Contratos</h2>
                        <ListContract contracts={contracts.data} deleteContract={this.delete} />
                    </div>
                </div>
            </div>
        );
    }

}