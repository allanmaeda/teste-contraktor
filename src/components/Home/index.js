import React, { Component } from 'react';
export default class Home extends Component {
    render() {

        return (
            <div className="text-center">
                <h2>Seja bem vindo,</h2><br />
                <h4>ao teste da implementação de um simples CRUD. </h4>
                <h5>Porém, com um enorme aprendizado, desafios e pressão.</h5><br />
                <h3>Obrigado pela oportunidade!!</h3><br />            
                <h3>E independente de tudo,</h3><br />            
                <h3><strong>SUCESSO</strong> à <span className="text-primary">Contraktor!!</span></h3>
            </div>
        )
    }
}