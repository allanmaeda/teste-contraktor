import React from 'react';
import Header from './components/Header';
import PartBox from './components/Parts'
import ContractBox from './components/Contracts'
import Home from './components/Home'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import './App.css'
function App() {
  return (
    <Router>
      <nav className="navbar navbar-expand-lg navbar-primary bg-primary sticky-top">
        <ul  className="navbar-nav mr-auto">
        <li><Link to={'/'}> <img className="ml-5 mr-5" src="https://secureservercdn.net/198.71.233.138/11n.b89.myftpupload.com/wp-content/uploads/2019/10/cropped-Logotipo-horizontal_1-bco.png?time=1573152225" width="220" height="50" alt=""/></Link></li>
          <li className="mt-3"><Link to={'/contract'} className="nav-link hover-list"> Contratos </Link></li>
          <li className="mt-3"><Link to={'/parts'} className="nav-link hover-list"> Partes </Link></li>
        </ul>
      </nav>
      <div className="container">
        <Header title="Gestão de Contratos" />
        <br />
      </div>
      <Switch>
        <Route exact path='/' component={Home} />
        <Route exact path='/contract' component={ContractBox} />
        <Route path='/parts' component={PartBox} />
      </Switch>
    </Router>
  );
}

export default App;
